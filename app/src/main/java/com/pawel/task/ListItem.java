package com.pawel.task;

/**
 * Created by pawel on 14.05.15.
 */
public class ListItem {
    private String title;
    private int thumbnail;

    public ListItem(String title, int thumbnail){
        setTitle(title);
        setThumbnail(thumbnail);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
