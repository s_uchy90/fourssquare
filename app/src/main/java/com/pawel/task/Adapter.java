package com.pawel.task;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by pawel on 14.05.15.
 */
public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private ListItem[] listItems;
    ViewHolder viewHolder;
    public Adapter(ListItem[] listItems) {
        this.listItems = listItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, null);

        viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.title.setText(listItems[position].getTitle());
        viewHolder.thumbnail.setImageResource(listItems[position].getThumbnail());
    }

    @Override
    public int getItemCount() {
        return listItems.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView thumbnail;

        public ViewHolder(View view) {
            super(view);
            this.thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            this.title = (TextView) view.findViewById(R.id.title);

        }
    }

}
