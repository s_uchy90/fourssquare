package com.pawel.task;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by pawel on 15.05.15.
 */
public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListItem itemsData[] = {new ListItem("Help", R.drawable.abc_btn_radio_material),
                new ListItem("Delete", R.mipmap.ic_launcher),
                new ListItem("Cloud", R.drawable.abc_btn_radio_material),
                new ListItem("Favorite", R.mipmap.ic_launcher),
                new ListItem("Like", R.drawable.abc_btn_radio_material),
                new ListItem("Rating", R.mipmap.ic_launcher)};


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Adapter mAdapter = new Adapter(itemsData);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
