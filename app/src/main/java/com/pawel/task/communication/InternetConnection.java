package com.pawel.task.communication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by pawel on 14.05.15.
 */
public class InternetConnection {
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Log.v("checking connection", "Internet Connection Not Present");
            return false;
        }
    }
}
