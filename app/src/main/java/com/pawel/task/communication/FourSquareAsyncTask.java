package com.pawel.task.communication;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

/**
 * Created by pawel on 14.05.15.
 */
public class FourSquareAsyncTask extends AsyncTask<String, Integer, Integer> {

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Integer doInBackground(String... params) {
        String data = ((new FourSquareClient()).getData());
        try {
            if(data !=null)
            JsonResponse.getPlaces(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.w("asyncTask", data);
        return 1;
    }

    @Override
    protected void onPostExecute(Integer result) {

    }
}
