package com.pawel.task.communication;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pawel on 14.05.15.
 */
public class FourSquareClient {
    private final String CLIENT_ID = "HN4JZHNH1MVOGAQF0KZ5CQ0JM5RHYJ35ITT4PVTALAU3UJSH";
    private final String CLIENT_SECRET = "JTYPHWBNMXRTVSMDQ0JPMPZNNAIGHUQJHLNCKAIYI2ZTYBMQ";
    private final String latitude = "51.758504";
    private final String longtitude = "19.4555722";

    private String api = "https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll=" + latitude + "," + longtitude + "&query=food";

    public String getData() {
        HttpURLConnection con = null;
        InputStream is = null;

        try {
            con = (HttpURLConnection) (new URL(api)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = br.readLine()) != null)
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();
            return buffer.toString();
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Throwable t) {
            }
            try {
                con.disconnect();
            } catch (Throwable t) {
            }
        }
        return null;
    }
}
