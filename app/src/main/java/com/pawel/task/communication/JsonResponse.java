package com.pawel.task.communication;

import com.pawel.task.object.FourSquarePlace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

/**
 * Created by pawel on 14.05.15.
 */
public class JsonResponse {

    public static ArrayList<FourSquarePlace> getPlaces(String response) throws JSONException {
        ArrayList<FourSquarePlace> places = new ArrayList<>();

        JSONObject jsonObj = (JSONObject) new JSONTokener(response).nextValue();
        JSONArray groups = (JSONArray) jsonObj.getJSONObject("response").getJSONArray("groups");

        int group_length = groups.length();
        if (group_length > 0) {
            for (int i = 0; i < group_length; i++) {
                JSONObject group = (JSONObject) groups.get(i);
                JSONArray items = (JSONArray) group.getJSONArray("items");

                int items_length = items.length();
                if (items_length > 0) {
                    for (int j = 0; j < items_length; j++) {
                        JSONObject item = (JSONObject) items.get(j);
                        JSONArray venues = (JSONArray) item.getJSONArray("venues");
                        int venues_length = items.length();
                        if (items_length > 0) {
                            for (int k = 0; k < venues_length; k++) {
                                JSONObject venue = (JSONObject) venues.get(j);
                                FourSquarePlace place = new FourSquarePlace();
                                place.setID(venue.getString("id"));
                                place.setName(venue.getString("name"));
                                JSONObject location = (JSONObject) venue.getJSONObject("location");
                                place.setAddress(location.getString("address"));
                                places.add(place);
                            }
                        }
                    }
                }
            }
        }
        return places;
    }
}
